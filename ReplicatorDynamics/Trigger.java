//*******************************************************
// Trigger.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*トリガー戦略．相手が裏切らない限り協調するが，一度でも裏切られれば以降は裏切り続ける戦略．*/

import java.util.*;

public class Trigger extends Strategy{
    private boolean trigger = false;
    public int nextMove() {
	if(countD == 0){
	    return Game.COOPERATION;
	} else {
	    return Game.DEFECTION;
	}
    }
}
