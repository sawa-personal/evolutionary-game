//*******************************************************
// ReplicatorDynamics.java
// created by Sawada tatsuki on 2017/11/27.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*レプリケータダイナミクスの差分方程式を解くプログラム．*/

import java.util.*;

public class ReplicatorDynamics{
    /*差分方程式はN[i] *= T[i] / TAve = T[i] / (ΣN[i]*T[i] / ΣN[i]) */
    public static void calc(double[] N, double[][] payoff, int SIZE) {
	//(1)総人口 ΣN[i] を計算する
	double NAll = 0; //総人口
	for(int i = 0; i < SIZE; i++){
	    NAll += N[i];
	}
	    
	//(2)グループiの適応度totalTを計算する
	double totalT[][] = new double[SIZE][SIZE]; //利得格納用
	for(int i = 0; i < SIZE; i++){
	    for(int j = 0; j < SIZE; j++){
		totalT[i][j] += N[j] * payoff[i][j];
	    }
	}
	    
	//(2)グループiの適応度の総和T[i]を計算する
	double T[] = new double[SIZE]; //利得格納用
	for(int i = 0; i < SIZE; i++){
	    T[i] = 0;
	    for(int j = 0; j < SIZE; j++){
		T[i] += totalT[i][j];
	    }
	}

	//(3)全グループの合計適応度 ΣT[i]*N[i] を計算する
	double NT = 0;
	for(int i = 0; i < SIZE; i++){
	    NT += N[i] * T[i];
	}
	    
	//(4)集団の平均適応度TAveを計算する
	double TAve = NT / NAll;
	    
	//(5)人口N[i]の更新
	for(int i = 0; i < SIZE; i++){
	    N[i] *= T[i] / TAve;
	}
    }
}
