//*******************************************************
// MajoD.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*相手のよく出す手を真似して出す．最初または同率の場合は裏切る．*/
public class MajoD extends Strategy{
    public int nextMove() {
	if(count == 0) {
	    return Game.DEFECTION;
	}
	if(countD / count >= 0.5) {
	    return Game.DEFECTION;
	}
	return Game.COOPERATION;
    }
}
