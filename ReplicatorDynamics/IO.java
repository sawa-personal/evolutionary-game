//*******************************************************
// IO.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;
import java.io.*;

public class IO{
    private String filename; //ファイル名
    private int lineNum; //行数
    
    public IO(String filename) {
	this.filename = filename;
	
	try{
	    File file = new File(filename); //ファイルインスタンス
	    FileReader filereader;	    
	    file.createNewFile(); //ファイルが存在しなければ作成
	    
	    filereader = new FileReader(file); //ファイル読込みインスタンス
	    LineNumberReader fin = new LineNumberReader(filereader); //行数取得用
	    while(null != fin.readLine()); //最後まで読む
	    lineNum = fin.getLineNumber(); //行数取得
	    fin.close();
	    filereader.close();
	}catch(IOException e){
	    System.out.println(e);
	}
    }

    //行数取得
    public int countLines(){
	return lineNum;
    }
    
    /*ファイル書込み*/
    public void write(String[][] mat, final int LEN1, final int LEN2){
	try{
	    File file = new File(filename); //ファイルインスタンス
	    FileWriter filewriter = new FileWriter(file); //ファイル書込みインスタンス

	    String out = "";
	    for(int i = 0; i < LEN1; i++){
		out += String.join(", ", mat[i]) + "\n";
	    }
	    filewriter.write(out); //書込み
	    filewriter.close();
	}catch(IOException e){
	    System.out.println(e);
	}
    }
    
    /*ファイル読込み*/
    public String[][] read(){
	String mat[][] = {};
	try{
	    File file = new File(filename); //ファイルインスタンス
	    FileReader filereader = new FileReader(file); //ファイル読込みインスタンス
	    BufferedReader br = new BufferedReader(filereader);
	    String lines[][] = new String[lineNum][];
	    
	    for(int i = 0; i < lineNum; i++){
		String line = br.readLine();
		line = line.replaceAll(" ", ""); //半角スペース削除
		String lineAry[] = line.split(",");
		lines[i] = lineAry;
	    }

	    filereader.close(); //閉じる
	    mat = lines;
	}catch(IOException e){
	    System.out.println(e);
	}
	return mat;
    }
}
