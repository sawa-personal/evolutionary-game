//*******************************************************
// DC.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*DCDC...と，常にDCを繰り替えす戦略*/
public class DC extends Strategy{
    public int nextMove() {
	if(count % 2 == 0) {
	    return Game.DEFECTION;
	}
	return Game.COOPERATION;
    }
}
