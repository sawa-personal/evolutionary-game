//*******************************************************
// DDC.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*DDCDDC...と，常にDDCを繰り替えす戦略*/
public class DDC extends Strategy{
    public int nextMove() {
	if(count % 2 == 0) {
	    return Game.DEFECTION;
	}
	if(count % 2 == 1) {
	    return Game.DEFECTION;
	}
	return Game.COOPERATION;
    }
}
