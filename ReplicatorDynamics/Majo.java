//*******************************************************
// Majo.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*多数決戦略．相手のよく出す手を真似して出す．最初または同率の場合は協調する．*/
public class Majo extends Strategy{
    public int nextMove() {
	if(count == 0) {
	    return Game.COOPERATION;
	}
	if(countD / count > 0.5) {
	    return Game.DEFECTION;
	}
	return Game.COOPERATION;
    }
}
