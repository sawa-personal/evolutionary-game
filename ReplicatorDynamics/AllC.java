//*******************************************************
// AllC.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*常に協調する戦略*/

import java.util.*;

public class AllC extends Strategy{
   public int nextMove() {
	return Game.COOPERATION; //協調する
    }
}
