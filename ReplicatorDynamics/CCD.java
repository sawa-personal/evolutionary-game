//*******************************************************
// CCD.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*CCDCCD...と，常にCCDを繰り替えす戦略*/
public class CCD extends Strategy{
    public int nextMove() {
	if(count % 2 == 0) {
	    return Game.COOPERATION;
	}
	if(count % 2 == 1) {
	    return Game.COOPERATION;
	}
	return Game.DEFECTION;
    }
}
