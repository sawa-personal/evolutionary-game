//*******************************************************
// TFTD.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*最初だけ裏切り，以降はTFT戦略．*/
public class TFTD extends Strategy{
    public int nextMove() {
	if(count == 0) {
	    return Game.DEFECTION;
	} else {
	    return opMove; //前回相手が出した手をそのまま出す
	}
    }
}
