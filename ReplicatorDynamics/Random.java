//*******************************************************
// Random.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*乱数戦略*/

import java.util.*;

public class Random extends Strategy{
    public int nextMove() {
	if (Math.random() < 0.5) {
	    return Game.COOPERATION; //半分は協調する
	} else {
	    return Game.DEFECTION; //半分は裏切る
	}
    }
}

