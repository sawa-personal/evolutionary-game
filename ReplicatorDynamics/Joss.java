//*******************************************************
// Joss.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*基本的にTFTと同じだが，協調すべきときに10%の確率で裏切る．*/
public class Joss extends Strategy{
    private double defectionRate = 0.1;
    public int nextMove() {
	if(opMove == Game.COOPERATION && Math.random() < defectionRate) {
	    return Game.DEFECTION;
	}
	return opMove;
    }
}
