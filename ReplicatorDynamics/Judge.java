//*******************************************************
// Judge.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*1対1の試合審判クラス*/

import java.util.*;

public class Judge{
    /*試合を実行する*/
    public void play(Strategy player1, Strategy player2) {
	//初期化する
	player1.reset();
	player2.reset();
	//指定回数試合を繰り返す
	for(int i = 0; i < Game.getCount(); i++){
	    deal(player1, player2);
	}
    }
    /*対戦を一度実行する*/
    private void deal(Strategy player1, Strategy player2) {
	//次の手を問い合わせる
	int move1 = player1.getNextMove();
	int move2 = player2.getNextMove();
	//選手に着手した手を教える
	player1.addHistory(move1, move2);
	player2.addHistory(move2, move1);
    }
}
