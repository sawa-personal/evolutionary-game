//*******************************************************
// TFTT.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*半返し戦略．相手が2回連続して裏切ったとき，1回だけ報復する*/

import java.util.*;

public class TFTT extends Strategy{
   public int nextMove() {
       if(opMove == Game.DEFECTION && opMove2 == Game.DEFECTION){
	   return Game.DEFECTION;
       } else {
	   return Game.COOPERATION;
       }
    }
}


