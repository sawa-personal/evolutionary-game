//*******************************************************
// Strategy.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ゲームをする戦略． */

import java.util.*;

public abstract class Strategy{
    protected int opMove; //相手の直前の手
    protected int opMove2; //相手の前前回の手
    protected int myMove; //自分の直前の手
    protected int myMove2; //自分の前前回の手
    protected int count; //試合の回数
    protected int countD; //裏切られた回数
    private int totalPoints; //総得点
    
    //コンストラクタ，初期化
    public Strategy(){
	reset();
    }
    
    //手の履歴を追加する
    public void addHistory(int myMove, int opMove) {
	totalPoints += Game.getPayoff(myMove, opMove); //合計利得を記録
	//裏切られた回数を記録
	if(opMove == Game.DEFECTION) {
	    this.countD++;
	}
	this.opMove2 = this.opMove; //相手の前前回の手を記録
	this.opMove = opMove; //相手の前回の手を記録
	this.myMove2 = this.myMove; //自分の前前回の手を記録
	this.myMove = myMove; //自分の前回の手を記録
	this.count++;
    }
    
    //次の手を参照する
    private static final double NOISE = -1;
    public int getNextMove() {
	int nextMove = nextMove();
	
	//ノイズ
	if(Math.random() < NOISE) {
	    if(nextMove == Game.COOPERATION) {
		nextMove = Game.DEFECTION;
	    } else {
		nextMove= Game.COOPERATION;
	    }
	}
	return nextMove;
    }
    //次の手
    public abstract int nextMove();
    
    //総得点を参照する
    public double getTotalPoints() {
	return totalPoints;
    }

    //初期化
    public void reset() {
    	totalPoints = 0;
	opMove = Game.COOPERATION;
	opMove2 = opMove;
	myMove = Game.COOPERATION;
	myMove2 = myMove;
	countD = 0;
	count = 0;
    }
}
