//*******************************************************
// Main.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*繰り返し囚人のジレンマのシミュレーション用メインクラス*/

import java.util.*;

public class Main{
    private static Strategy player1[]; //行プレイヤー
    private static Strategy player2[]; //列プレイヤー
    private static double payoff[][]; //繰り返しの中での利得行列

    //戦略
    private static Strategy[] strategies(){
	Strategy strategies[] = {
	    new AllC(), //善人戦略
	    new AllD(), //悪人戦略
	    new TFT(), //しっぺ返し戦略
	    new TFTT(), //半返し戦略
	    new TTFAT(), //倍返し戦略
	    new Trigger(), //トリガー戦略
	    new Pavlov(), //パブロフ戦略
	    new Random(), //乱数戦略
	    new CD(), //CD戦略
	    new DC(), //DC戦略
	    new CCD(), //CCD戦略
	    new DDC(), //DDC戦略
	    new TFTD(), //最初は裏切るしっぺ返し戦略
	    new ATFT(), //逆しっぺ返し戦略
	    new Joss(), //10%の確率で裏切るしっぺ返し戦略
	    new Tester(), //試し屋戦略
	    new Majo(), //多数決戦略
	    new MajoD(), //最初は裏切る多数決戦略
	};
	return strategies;
    }
    private static int SIZE; //戦略の数
    
    public static void main(String[] args){
	// 大会
	registerPlayers(); //選手を作成
	play(); //大会を実施
	
	//自然淘汰
	selection(); //淘汰を実施

	//遺伝的アルゴリズム
	ga(); //淘汰を実施
    }

    /******************** start: 大会に関する処理 ********************/
    //選手を登録する
    private static void registerPlayers() {
	player1 = strategies();
	player2 = strategies();
	SIZE = player1.length;
    }
    
    //競技を実施する．総当たり戦
    private static void play() {
	payoff = new double[SIZE][SIZE]; //利得行列
	Judge judge = new Judge(); //審判
	
	for(int i = 0; i < SIZE; i++){
	    for(int j = 0; j < SIZE; j++){
		judge.play(player1[i], player2[j]);
		showPayoff(i, j); //結果発表
		payoff[i][j] = player1[i].getTotalPoints() / Game.getCount(); //平均利得
	    }
	}
    }
    
    //結果発表
    private static void showPayoff(int i, int j){
	double total = player1[i].getTotalPoints() / Game.getCount(); //行プレイヤーの1試合あたりの利得
	System.out.printf("%6.2f%s", total, j == SIZE - 1 ? "\n" : ", ");
    }
    /******************** end: 大会に関する処理 ********************/
    
    /******************** start: 自然淘汰に関する処理 ********************/
    private static final int GENERATION = 100; //シミュレーションを行う最大世代数
    private static final double POPULATION = 100; //初期人口
    
    //自然淘汰
    private static void selection() {
	double N[] = new double[SIZE]; //各グループの人口格納用
	//人口の初期化
	for(int i = 0; i < SIZE; i++){
	    N[i] = POPULATION;
	}
	showPopulation(0, N);  //初期人口の発表

	//自然淘汰を実施
	for(int g = 1; g <= GENERATION; g++){
	    ReplicatorDynamics.calc(N, payoff, SIZE); //レプリケータダイナミクスの計算
	    showPopulation(g, N); //人口の発表
	}
    }

    //人口の発表
    private static void showPopulation(int g, double N[]){
	System.out.printf("%3d, ", g);
	for(int i = 0; i < SIZE; i++){
	    System.out.printf("%6.2f%s", N[i], i == SIZE - 1 ? "\n" : ", ");
	}
    }
    /******************** end: 自然淘汰に関する処理 ********************/

    /******************** start: 遺伝的アルゴリズムに関する処理 ********************/
    private static void ga() {

	
    }
    /******************** end: 遺伝的アルゴリズムに関する処理 ********************/
}
