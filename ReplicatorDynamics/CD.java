//*******************************************************
// CD.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*CDCD...と，常にCDを繰り替えす戦略*/
public class CD extends Strategy{
    public int nextMove() {
	if(count % 2 == 0) {
	    return Game.COOPERATION;
	}
	return Game.DEFECTION;
    }
}
