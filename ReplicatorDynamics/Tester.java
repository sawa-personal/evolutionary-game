//*******************************************************
// Tester.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*試し屋戦略．最初の3手はDCCで固定．相手の2手目がDなら，それ以降はTFT，相手の2手目がCなら，それ以降はDCを繰り返す戦略．*/
public class Tester extends Strategy{
    private boolean cOrD;
    public int nextMove() {
	if(count == 0) {
	    return Game.DEFECTION;
	}
	if(count == 1) {
	    return Game.COOPERATION;
	}
	if(count == 2) {
	    if(opMove == Game.COOPERATION) {
		cOrD = true;
	    } else {
		cOrD = false;
	    }
	    return Game.COOPERATION;
	}

	if(cOrD == true){
	    if(count % 2 == 0) {
		return Game.DEFECTION;
	    } else {
		return Game.COOPERATION;
	    }
	} else {
	    return opMove;
	}
    }
}
