//*******************************************************
// Pavlov.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*パブロフ戦略．最初は協調し，以後，得点が高ければ(CC or DCならば)同じ手，低ければ(CD or DDならば)手を変える*/

import java.util.*;

public class Pavlov extends Strategy{
   public int nextMove() {
       if(opMove == myMove){
	   return Game.COOPERATION; //相手と自分が同じ手だったら協調
       } else {
	   return Game.DEFECTION; //相手と自分が違う手だったら裏切り
       }
    }
}
