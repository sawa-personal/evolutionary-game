//*******************************************************
// ATFT.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/*逆しっぺ返し戦略．最初は裏切り，以後は相手が前回出した手と逆の手を次回自分が出す戦略．*/
public class ATFT extends Strategy{
    public int nextMove() {
	if(count == 0) {
	    return Game.DEFECTION;
	} else {
	    return opMove;
	}
    }
}
