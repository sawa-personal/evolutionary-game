//*******************************************************
// TTFAT.java
// created by Sawada tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*倍返し戦略．相手が1度裏切ると，2回報復するという戦略*/
import java.util.*;

public class TTFAT extends Strategy{
   public int nextMove() {
       if(opMove == Game.DEFECTION || opMove2 == Game.DEFECTION){
	   return Game.DEFECTION;
       } else {
	   return Game.COOPERATION;
       }
    }
}
