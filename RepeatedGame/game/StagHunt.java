//*******************************************************
// StagHunt.java
// created by Sawada Tatsuki on 2018/02/09.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 鹿狩りゲーム */

import java.util.*;

public class StagHunt extends Game {
    //利得表を参照する 
    public double[][] getPayoff() {
	private double payoff[][] = {
	    {3, 0},
	    {2, 1}
	};
	return payoff;
    }
}
