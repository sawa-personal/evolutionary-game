//*******************************************************
// Game.java
// created by Sawada Tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
import java.util.*;

/* 囚人のジレンマゲーム */
public abstract class Game {
    //利得行列を参照:オーバーライド前提
    public abstract double getPayoff(int myMove, int opMove);
}
