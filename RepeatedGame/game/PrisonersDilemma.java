//*******************************************************
// PrisonersDilemma.java
// created by Sawada Tatsuki on 2017/12/26.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 囚人のジレンマ */

import java.util.*;

public class PrisonersDilemma extends Game {
    //利得表を参照する    
    public double getPayoff(int myMove, int opMove) {
	double[][] payoff = {
	    {8, 0},
	    {2, 6}
	};
	return payoff[myMove][opMove];
    }
}
