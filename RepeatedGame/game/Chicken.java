//*******************************************************
// Chicken.java
// created by Sawada Tatsuki on 2017/12/26.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* チキンゲーム(a.k.a. タカハトゲーム，スノードリフトゲーム) */

import java.util.*;

public class Chicken extends Game{
    //利得表を参照する 
    public double[][] getPayoff() {
	private double payoff[][] = {
	    {0, -1},
	    {1, -20}
	};

	/*
	  //タカハトゲーム(ESS)
	  double V = 50; //勝利時に得る利得
	  double C = 100; //傷付いた時の損失
	  private double payoff[][] = {
	      {V / 2, 0},
	      {V, (V - C) / 2}
	  };
	 */
	return payoff;
    }
}
