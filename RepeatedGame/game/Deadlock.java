//*******************************************************
// Deadlock.java
// created by Sawada Tatsuki on 2018/02/09.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 行き詰まりゲーム */

import java.util.*;

public class Deadlock extends Game {
    //利得表を参照する 
    public double[][] getPayoff() {
	private double payoff[][] = {
	    {1, 0},
	    {3, 2}
	};
	return payoff;
    }
}
