//*******************************************************
// Strategy.java
// created by Sawada Tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ゲームをする戦略 */

import java.util.*;

public abstract class Strategy {
    public static final int COOPERATION = 0; //協調
    public static final int DEFECTION = 1; //裏切り

    protected int opMove; //相手の直前の手
    protected int myMove; //自分の直前の手
    private double fitness; //適応度＝総得点
    
    //コンストラクタ
    public Strategy(){
	reset();
    }
    
    //手の履歴を追加する
    public void addHistory(int myMove, int opMove, double payoff) {
	fitness += payoff;
	this.opMove = opMove;
	this.myMove = myMove;
    }
    
    //次の手を参照する
    public abstract int getNextMove();
    
    //総得点を参照する
    public double getFitness() {
	return fitness;
    }
    
    //初期化
    public void reset() {
	fitness = 0;
	opMove = COOPERATION;
    }
}
