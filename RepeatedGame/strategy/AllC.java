//*******************************************************
// AllC.java
// created by Sawada Tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 常に協調する戦略 */

import java.util.*;

public class AllC extends Strategy {
   public int getNextMove() {
	return COOPERATION; //協調する
    }
}
