//*******************************************************
// Random.java
// created by Sawada Tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 乱数戦略 */

import java.util.*;

public class Random extends Strategy {
    public int getNextMove() {
	if (Math.random() < 0.5) {
	    return COOPERATION; //半分は協調する
	} else {
	    return DEFECTION; //半分は裏切る
	}
    }
}
