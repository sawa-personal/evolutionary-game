//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 繰り返しゲームの期待利得を求める */

import java.util.*;

public class Main {
    //登録選手
    private static Strategy player1[];
    private static Strategy player2[];

    public static int nCount = 100; //対戦回数
    public static Game pdGame = new PrisonersDilemma(); //囚人のジレンマ
    
    //main処理
    public static void main(String[] args){
	registerPlayers(); //選手を登録する
	play(); //総当たり戦を実施する
    }

    //総当たり戦を実施する
    private static void play() {
	Judge judge = new Judge(); //審判
	for(int i = 0; i < player1.length; i++) {
	    for(int j = 0; j < player2.length; j++) {
		judge.play(player1[i], player2[j], pdGame);
		showResult(i, j); //結果発表
	    }
	}
    }
    
    //選手を登録する
    private static void registerPlayers() {
	player1 = new Strategy[4];
	player2 = new Strategy[4];
	player1[0] = new AllC(); //常に協調
	player1[1] = new AllD(); //常に裏切り
	player1[2] = new Random(); //強調，裏切りランダム
	player1[3] = new TFT(); //しっぺ返し
	player2[0] = new AllC();
	player2[1] = new AllD();
	player2[2] = new Random();
	player2[3] = new TFT();
    }
    
    //結果を発表する
    private static void showResult(int i, int j) {
	double fitness1 = player1[i].getFitness() / nCount;
	double fitness2 = player2[i].getFitness() / nCount;
	System.out.printf("player%d .vs. player%d\t%6.2f:%6.2f\n",i, j, fitness1, fitness2);
    }
}
