//*******************************************************
// Judge.java
// created by Sawada Tatsuki on 2017/11/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 1対1の試合審判クラス */

import java.util.*;

public class Judge {
    //試合を実行
    public void play(Strategy player1, Strategy player2, Game game) {
	//初期化
	player1.reset();
	player2.reset();
	//指定回数だけ試合を繰り返す
	for(int i = 0; i < Main.nCount; i++) {
	    deal(player1, player2, game);
	}
    }
    
    //対戦を一度実行する
    private void deal(Strategy player1, Strategy player2, Game game) {
	//次の手を問い合わせる
	int move1 = player1.getNextMove();
	int move2 = player2.getNextMove();
	double fitness1 = game.getPayoff(move1, move2);
	double fitness2 = game.getPayoff(move2, move1);
	//選手に着手した手を教える
	player1.addHistory(move1, move2, fitness1);
	player2.addHistory(move2, move1, fitness2);
    }
}
