//*******************************************************
// NetworkReciprocity.java
// created by Sawada tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ネットワーク相互作用． */

import java.util.*;

public class NetworkReciprocity {
    private static double b; //協力にる利得
    private static double c; //協力による損失
    private static double H; //
    
    private static final int payoff[][] = {
	{b - c, H - c},
	{b - H, 0}
    };
}
