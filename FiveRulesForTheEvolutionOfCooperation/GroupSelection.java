//*******************************************************
// GroupSelection.java
// created by Sawada tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* グループ選択． */

import java.util.*;

public class GroupSelection {
    private static double b; //協力による利得
    private static double c; //協力による損失
    private static double m; //
    private static double n; //
    
    private static final int payoff[][] = {
	{(b - c) * (m + n), (b - c) * m - c * n},
	{b * n, 0}
    };
}
