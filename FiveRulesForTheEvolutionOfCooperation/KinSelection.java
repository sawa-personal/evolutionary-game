//*******************************************************
// KinSelection.java
// created by Sawada tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 血縁選択． */

import java.util.*;

public class KinSelection {
    private static double b; //協力にる利得
    private static double c; //協力による損失
    private static double r; //血縁関係度
    
    private static final int payoff[][] = {
	{(b - c) * (1 + r), b * r - c},
	{b - r * c, 0}
    };
}
