//*******************************************************
// DirectReciprocity.java
// created by Sawada tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 直接相互作用． */

import java.util.*;

public class DirectReciprocity {
    private static double b; //協力にる利得
    private static double c; //協力による損失
    private static double w; //
    
    private static final int payoff[][] = {
	{(b - c) * (1 + w), -c},
	{b, 0}
    };
}
