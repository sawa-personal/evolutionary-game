//*******************************************************
// Game.java
// created by Sawada tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/*5つのルールにおけるゲーム*/

import java.util.*;

public class Game{
    public static final int COOPERATION = 0; //協調．利得行列の1行(列)目
    public static final int DEFECTION = 1; //裏切り．利得行列の2行(列)目．

    private static int nCount = 100; //対戦回数
    
    //利得行列
    private static final int payoff[][] = {
	{6, 0},
	{8, 2}
    };
    
    //対戦回数を参照する
    public static int getCount() {
	return nCount;
    }
    
    //利得表を参照する
    public static int getPayoff(int myMove, int opMove){
	return payoff[myMove][opMove];
    }
}
